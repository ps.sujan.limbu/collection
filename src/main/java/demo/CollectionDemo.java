package demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class CollectionDemo{
    public static void main(String[] args) {
        //creating a ArrayList(from Collection Interface) of String type
        List<String> studentName = new ArrayList<>();

        /*Manipulation of the Collection(ArrayList)*/
        //Adding items in the List
        studentName.add("Ramu");
        studentName.add("Shyamu");
        studentName.add("Khyamu");
        studentName.add("Hemu");
        System.out.println("List after Adding items: "+ studentName);

        //Update items in the List
        studentName.set(0, "updated Ramu");
        System.out.println("List after Update: "+ studentName);

        /*COllections class utility method implementation*/
        //Sorting list using sort() method from Collection class
        Collections.sort(studentName);
        System.out.println("List after sorting: "+ studentName);

        Collections.addAll(studentName, "Sujan","Limbu");
        System.out.println("List after Add : "+ studentName);

        //Searching the index of an item in the list
        System.out.println("Index Searched of Khyamu is: "+Collections.binarySearch(studentName, "Khyamu"));
        //Minimun item in the list
        System.out.println("Min Item: "+Collections.min(studentName));

        //Reversing the order of items in the List
        Collections.reverse(studentName);
        System.out.println("After reverse: "+studentName);
        //Minimun item in the list
        System.out.println("Min Item: "+Collections.min(studentName));

        //Swap items in the List
        Collections.swap(studentName, 0, 2);
        System.out.println("After swaping the item in the index 0 & 2: "+studentName);

    }
}