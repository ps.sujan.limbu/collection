package demo;

import java.util.HashSet;
import java.util.Set;

class SetImpl{
    public static void main(String[] args) {
        Set<Integer> oddNumbers = new HashSet<>();
        oddNumbers.add(1);
        oddNumbers.add(3);
        oddNumbers.add(7);
        oddNumbers.add(5);
        oddNumbers.add(9);
        System.out.println("Odd Set: "+oddNumbers);
        oddNumbers.add(2);//duplicate item

        Set<Integer> evenNumbers = Set.of(2,4,6,8,10);
        System.out.println("Even Set: "+evenNumbers);

        //Add all data in HashSet
        Set<Integer> numbers = new HashSet<>();
        numbers.addAll(oddNumbers);
        numbers.addAll(evenNumbers);
        System.out.println("Adding all even and odd numbers: "+numbers);

        //Remove All
        numbers.removeAll(Set.of(5,10,15));
        System.out.println("After removing a set of numbers: "+numbers);

        //Retain elements or find intersection between two sets
        numbers.retainAll(Set.of(3,6,9,12,15));
        System.out.println("After retaining a set of numbers: "+numbers);
        
        //check if empty
        System.out.println("Is numbers Empty : "+numbers.isEmpty());
        numbers.clear();
        System.out.println("Is numbers Empty(after clear()) : "+numbers.isEmpty());
        System.out.println("Final numbers : "+numbers);



    }

}