package demo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class MapDemo{
    public static void main(String[] args) {
        Map<Integer, String> students = new HashMap<>();
        students.put(001, "Sujan Limbu");
        students.put(002, "pujan zimbu");
        students.put(003, "Rujan timbu");
        System.out.println(students);
        // return keys using keySet()
        System.out.println("Keys: " + students.keySet());

        // return all values using values()
        System.out.println("Values: " + students.values());

        // key-value association using entrySet()
        System.out.println("Key/Value mappings: " + students.entrySet());

        //Get item by key
        System.out.println("Value of key 001: "+students.get(001));

        //Search by key
        System.out.println("search 001: "+ students.containsKey(001));
        System.out.println("search 010: "+ students.containsKey(010));
        //Search by value
        System.out.println("search 'sujan': "+ students.containsValue("sujan"));
        System.out.println("search 'Sujan Limbu': "+ students.containsValue("Sujan Limbu"));

        //iterate values in the map
        Iterator<Integer> studentItr = students.keySet().iterator();
        while(studentItr.hasNext()){
            System.out.println(students.get(studentItr.next()));
        }

        //Remove an item
        System.out.println("Removed item: " +students.remove(001));
        System.out.println("after removing item: "+ students);
        
        //Replace a value in the map
        students.replace(002, "pujan zimbu", "newValue");
        System.out.println("after replacing value: "+ students);
        students.replace(002, "pujan zimbu");
        System.out.println("after re-replacing value: "+ students);

        students.clear();
        System.out.println("after clear(): "+ students);
    }
}