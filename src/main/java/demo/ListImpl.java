package demo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ListImp{
    public static void main(String[] args) {
        List<String> studentName = new ArrayList<>();

        //Adding items in arrayList
        studentName.add("Khyamu");
        studentName.add("Hemu");
        studentName.add("Ramu");
        studentName.add("Shyamu");
        studentName.add(1, "element");
        System.out.println("List after Adding items: "+ studentName);

        //Get item in the ArrayList by index
        System.out.println("Item at index 1: "+ studentName.get(1));

        //Adding all items from another list
        studentName.addAll(getAnotherList());
        System.out.println("List after Adding Another list: "+ studentName);

        //Iterate items in the List
        Iterator<String> studentItr = studentName.iterator();
        while(studentItr.hasNext()){
            System.out.println(studentItr.next());
        }

        //Searching if an item exists
        System.out.println("Is 'sujan' in list? "+ studentName.contains("sujan"));
        System.out.println("Is 'sam' in list? "+ studentName.contains("sam"));

        //Remove an item
        studentName.remove(1);
        System.out.println("After removing item at index 1: "+ studentName);

    }
    private static List<String> getAnotherList(){
        List<String> tempList = new ArrayList<>();
        tempList.add("sam");
        tempList.add("john");
        return tempList;
    }
}