import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

class ArrayImpl{
    public static void main(String[] args) {
        String[] names = {"sujan","pujan","rujan","avi"};
/*
        String[] sur = new String[2];
        sur[0] = "limbu";
        sur[1] = "rai";

        Student[] students = new Student[2];
        students[0] = new Student(001, "Sujan Limbu");
        students[1] = new Student(002, "Pujan rai");
*/
        printNames(names);;
        System.out.println("at 0 index: "+names[0]);
        System.out.println("Arary lenght: "+names.length); 

        //converting array to ArrayList
        ArrayList<String> nameAsList = new ArrayList<>(Arrays.asList(names));
        System.out.println("Array as List: "+nameAsList);

        //copying the array elements to another array
        String[] nameCopy = Arrays.copyOf(names, names.length);//increase length and add items as names[indx] = "";
        System.out.println(Arrays.asList(nameCopy));

        //equal
        System.out.println("Is names and  namesCopy array equal? "+Arrays.equals(names, nameCopy));

        //hashCodee
        System.out.println("Hash Code of array names: "+Arrays.hashCode(names));
        System.out.println("Hash Code of array nameCopy: "+Arrays.hashCode(nameCopy));

        //toString
        System.out.println("Array as string: "+Arrays.toString(names));

        //setAll, update all elements in array using generator function
        Arrays.setAll(nameCopy,name->"xxxxx");
        System.out.println("names set to xxxxx"+Arrays.toString(nameCopy));

        //Arrays.stream() to convert array into Stream
        Stream<String> stream = Arrays.stream(names);
        stream.forEach(name -> System.out.println("->"+name));

        // sorting the array
        Arrays.sort(names);
        System.out.println("Sorted array: "+ Arrays.toString(names));

        //Binary search(array needs to be sorted for binary search)
        System.out.println("'avi' found at index: " + Arrays.binarySearch(names, "avi"));

    }

    private static void printNames(String[] names){
        for(int i = 0 ; i<names.length; i++){
            System.out.println(names[i]);
        }
    }

    /*
    public String[] removeItemFromArray(String[] names, int indexToDelete){
        String[] newNames = new String[names.length - 1];
    
        for (int i = 0, j = 0; i < names.length; i++) {
            if (i == indexToDelete) {
                continue;
            }
            newNames[j++] = names[i];
        }
        return newNames;
    }

    //adding new item in array by creating new array
    public void addItemsInArray(String[] names){
        String[] newNames = Arrays.copyOf(names, names.length+1);
        newNames[newNames.length-1] = "dujan";
        System.out.println("new array : "+ Arrays.toString(names));
    }
    */
}

class Student{
    private int rollNo;
    private String name;
    Student(int rollNo, String name)
    {
        this.rollNo = rollNo;
        this.name = name;
    }    
}